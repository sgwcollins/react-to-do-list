import { useState, useEffect } from 'react';
import moment from 'moment';
import { firebase } from '../firebase';
import { collatedTasksExist } from '../helpers';


export const useTasks = (selectedProject, searchText = '', filterStatus = '') => {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    let unsubscribe = firebase
      .firestore()
      .collection('tasks')
      .where('userId', '==', '1');

    if (selectedProject && !collatedTasksExist(selectedProject)) {
      unsubscribe = unsubscribe.where('projectId', '==', selectedProject);
    } else if (selectedProject === 'TODAY') {
      unsubscribe = unsubscribe.where('date', '==', moment().format('YYYY-MM-DD'));
    }


    unsubscribe = unsubscribe.onSnapshot((snapshot) => {
      let newTasks = snapshot.docs.map((task) => ({
        id: task.id,
        ...task.data(),
      }));

      newTasks = filterStatus ? newTasks.filter((task) => task.status === filterStatus) : newTasks

      setTasks(
        selectedProject === 'NEXT_7'
          ? newTasks.filter(
            (task) => moment(task.date, 'YYYY-MM-DD').diff(moment(), 'days') <= 7
                        && task.archived !== true,
          ).filter((task) => (`${task.task} ${task.taskTitle}`).includes(searchText))
          : newTasks.filter((task) => task.archived !== true)
            .filter((task) => (`${task.task} ${task.taskTitle}`).includes(searchText)),

      );

    });

    return () => unsubscribe();
  }, [selectedProject, searchText, filterStatus]);


  return { tasks};
};

export const useProjects = () => {
  const [projects, setProjects] = useState([]);


  useEffect(() => {
    firebase
      .firestore()
      .collection('projects')
      .where('userId', '==', '1')
      .orderBy('projectId')
      .get()
      .then((snapshot) => {
        const allProjects = snapshot.docs.map((project) => ({
          ...project.data(),
          docId: project.id,
        }));

        if (JSON.stringify(allProjects) !== JSON.stringify(projects)) {
          setProjects(allProjects);
        }
      });
  }, [projects]);

  return { projects, setProjects };
};
