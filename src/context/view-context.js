import React, { createContext, useContext, useState } from 'react';


export const ViewTasksContext = createContext();

export const ViewTasksProvider = ({ children }) => {

    const [view, setView] = useState('tasks');

    return (
        <ViewTasksContext.Provider
            value={{ view, setView }}
        >
            {children}
        </ViewTasksContext.Provider>
    );
};

export const useViewTasksValue = () => useContext(ViewTasksContext);
