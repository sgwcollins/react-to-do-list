import React from 'react';
import {
  ProjectsContext,
  ProjectProvider,
  useProjectsValue,
} from './project-context';

import {
  SelectedProjectContext,
  SelectedProjectProvider,
  useSelectedProjectsValue,
} from './selected-project-context';

import {
  SearchTasksContext,
  SearchTasksProvider,
  useSearchTasksValue,
} from './search-context';

import {
  ViewTasksContext,
  ViewTasksProvider,
  useViewTasksValue,
} from './view-context';


import {
  FilterTasksContext,
  FilterTasksProvider,
  useFilterTasksValue,
} from './filter-context';

export {
  ProjectsContext,
  ProjectProvider,
  useProjectsValue,
  SelectedProjectContext,
  SelectedProjectProvider,
  useSelectedProjectsValue,
  SearchTasksContext,
  SearchTasksProvider,
  useSearchTasksValue,
  ViewTasksContext,
  ViewTasksProvider,
  useViewTasksValue,
  FilterTasksContext,
  FilterTasksProvider,
  useFilterTasksValue,
};
