import React, { createContext, useContext, useState } from 'react';


export const FilterTasksContext = createContext();

export const FilterTasksProvider = ({ children }) => {
  const [filter, setFilter] = useState('');

  return (
    <FilterTasksContext.Provider
      value={{ filter, setFilter }}
    >
      {children}
    </FilterTasksContext.Provider>
  );
};

export const useFilterTasksValue = () => useContext(FilterTasksContext);
