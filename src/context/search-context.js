import React, { createContext, useContext, useState } from 'react';


export const SearchTasksContext = createContext();

export const SearchTasksProvider = ({ children }) => {
  const [search, setSearch] = useState('');

  return (
    <SearchTasksContext.Provider
      value={{ search, setSearch }}
    >
      {children}
    </SearchTasksContext.Provider>
  );
};

export const useSearchTasksValue = () => useContext(SearchTasksContext);
