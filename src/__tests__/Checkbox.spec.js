import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { CheckboxT } from '../components/Checkbox';

beforeEach(cleanup);

jest.mock('../firebase', () => ({
  firebase: {
    firestore: jest.fn(() => {
      jest.fn(() => {
        jest.fn(() => {
          jest.fn();
        });
      });
    }),
  },
}));


describe('<Checkbox />', () => {
  describe('Success', () => {
    it('renders the task checkbox', () => {
      const { queryByTestId, debug } = render(
        <CheckboxT id="1" taskDesc="Finish" />,
      );
      expect(queryByTestId('checkbox-action')).toBeTruthy();
    });

    it('renders the task checkbox and simulates and click', () => {
      const { queryByTestId, debug } = render(
        <CheckboxT id="1" taskDesc="Finish" />,
      );

      expect(queryByTestId('checkbox-action')).toBeTruthy();
      fireEvent.click(queryByTestId('checkbox-action'));
    });

  });
});
