import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { AddTasks } from '../components/AddTasks';

beforeEach(cleanup);


describe('<AddTasks />', () => {
  describe('Success', () => {
    it('renders add tasks', () => {
      const { queryByTestId, debug } = render(
        <AddTasks />,
      );

      expect(queryByTestId('add-task-comp')).toBeTruthy();
    });

    it('renders add tasks form', () => {
      const setShowMain = jest.fn();

      const { queryByTestId, debug } = render(
        <AddTasks />,
      );

      fireEvent.click(queryByTestId('show-main-action'));
      expect(setShowMain).toHaveBeenCalled();
    });
  });
});
