import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { App } from '../App';

beforeEach(cleanup);


describe('<App />', () => {
  describe('Success', () => {
    it('renders application', () => {
      const { queryByTestId, debug } = render(
        <App />,
      );
      expect(queryByTestId('application')).toBeTruthy();
    });
  });
});
