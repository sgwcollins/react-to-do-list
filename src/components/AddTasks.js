import React, { useState } from 'react';
import { HandleTasks } from './HandleTasks';

export const AddTasks = () => {
  const [showMain, setShowMain] = useState(false);

  return (
    <div
      className="add-task"
      data-testid="add-task-comp"
    >
      <div
        className="add-task__shallow"
        data-testid="show-main-action"
        onClick={() => setShowMain(!showMain)}
      >
        <span className="add-task__plus">+</span>
        <span className="add-task__test">Add Task</span>

      </div>
      {(showMain) && (
      <HandleTasks
        setShowMain={setShowMain}
        editMode="add"
      />

      )}
    </div>
  );
};
