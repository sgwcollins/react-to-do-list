import React from 'react';
import { Tooltip } from 'antd';
import { FaCircle } from 'react-icons/fa';
import { statusList } from '../constants';

export const StatusBullet = ({ taskStatus = 'NOT_STARTED' }) => {
  const status = statusList.find((x) => x.key === taskStatus);
  return (
    <span className="bullet-holder">
      <Tooltip title={status.name}>
        <FaCircle style={{ color: status.color }} />
      </Tooltip>
    </span>
  );
};
