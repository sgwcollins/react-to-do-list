import React from 'react';
import { Checkbox } from 'antd';
import { firebase } from '../firebase';

export const CheckboxT = ({ id }) => {
  const archieveTask = () => {
    setTimeout((
      () => {
        firebase
          .firestore()
          .collection('tasks')
          .doc(id)
          .update({
            archived: true,
          });
      }
    ), 200);
  };

  return (
    <div
      className="checkbox-holder"
    >
      <Checkbox data-testid="checkbox-action" onChange={() => archieveTask()} />
    </div>

  );
};
