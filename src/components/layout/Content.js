import React from 'react';
import { Sidebar } from './SideBar';
import { Tasks } from '../Tasks';

export const Content = () => (
  <>
    <Sidebar />
    <Tasks />
  </>
);
