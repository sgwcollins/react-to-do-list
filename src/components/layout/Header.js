import React from 'react';

export const Header = () => (
  <header className="header" data-testid="header">
    <nav>
        <div className="logo">
            <img src="https://join.northone.com/static/media/NorthOne-Color.75a736dd.svg" className="css-ek3ii5" />
        </div>
    </nav>
  </header>

);
