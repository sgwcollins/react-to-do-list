import React, { useState } from 'react';
import {
  FaInbox,
  FaRegCalendarAlt,
  FaRegCalendar,
} from 'react-icons/fa';
import { Projects } from '../Projects';
import { useSelectedProjectsValue, useViewTasksValue } from '../../context';
import { AddProject } from '../AddProject';

export const Sidebar = () => {
  const { setSelectedProjects } = useSelectedProjectsValue();
  const { setView } = useViewTasksValue();
  const [active, setActive] = useState('inbox');
  const [showProjects, setShowProjects] = useState(true);


  return (
    <aside>
      <div className="sidebar" data-testid="sideBar">
        <ul className="sidebar__generic">
          <li>
            <div
              data-testid="inbox-action"
              aria-label="Show inbox tasks"
              tabIndex={0}
              role="button"
              onClick={() => {
                setView('tasks');
                setActive('inbox');
                setSelectedProjects('ALL');
              }}
              onKeyDown={() => {
                setView('tasks');
                setActive('inbox');
                setSelectedProjects('ALL');
              }}
            >
              <span>
                <FaInbox />
              </span>
              <span>
                All
              </span>
            </div>
          </li>
          <li>
            <div
              data-testid="today-action"
              aria-label="Show today's tasks"
              tabIndex={0}
              role="button"
              onClick={() => {
                setView('tasks');
                setActive('today');
                setSelectedProjects('TODAY');
              }}
              onKeyDown={() => {
                setView('tasks');
                setActive('today');
                setSelectedProjects('TODAY');
              }}
            >
              <span>
                <FaRegCalendar />
              </span>
              <span>
                Due Today
              </span>
            </div>
          </li>
          <li>
            <div
              data-testid="today-action"
              aria-label="Show today's tasks"
              tabIndex={0}
              role="button"
              onClick={() => {
                setView('tasks');
                setActive('today');
                setSelectedProjects('NEXT_7');
              }}
              onKeyDown={() => {
                setView('tasks');
                setActive('today');
                setSelectedProjects('NEXT_7');
              }}
            >
              <span>
                <FaRegCalendarAlt />
              </span>
              <span>
                Due in the next 7 days
              </span>
            </div>
          </li>
          <li>
            <div
              data-testid="today-action"
              aria-label="Show today's tasks"
              tabIndex={0}
              role="button"
              onClick={() => {
                setView('calendar');
              }}
              onKeyDown={() => {
                setView('calendar');
              }}
            >
              <span>
                <FaRegCalendarAlt />
              </span>
              <span>
                Calendar View
              </span>
            </div>
          </li>
        </ul>

        <div className="sidebar__middle">
          <span>
            <h2>Projects</h2>
          </span>
        </div>
        <ul className="sidebar__projects">
          { showProjects && <Projects /> }
        </ul>
        <AddProject />
      </div>
    </aside>
  );
};
