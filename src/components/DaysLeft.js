import React from 'react';
import moment from 'moment';
import {Tooltip} from "antd";


export const DaysLeft = ({ date = moment() }) => {

  const diff = moment(date, 'YYYY-MM-DD').diff(moment(), 'days');

  let daysLeft = '';
  if (diff === 0) {
    daysLeft = 'Today is the Due Date';
  } else if (diff < 0) {
    daysLeft = 'Task is passed deadline!';
  } else {
    daysLeft = `${diff} days left`;
  }
  return (
    <span>
      <Tooltip title={`Due Date is ${date}`}>
        {daysLeft}
      </Tooltip>
    </span>
  );
};
