import React, { useEffect, useState } from 'react';
import { useTasks } from '../hooks';
import { AddTasks } from './AddTasks';
import { collatedTasks } from '../constants';
import { getTitle, getCollatedTitle, collatedTasksExist } from '../helpers';
import {
  useSelectedProjectsValue,
  useProjectsValue,
  useSearchTasksValue,
  useViewTasksValue,
  useFilterTasksValue,
} from '../context';


import { IndiviualTasks } from './IndiviualTasks';
import { SearchTasks } from './Search';
import { FilterTasks } from './Filter';
import { CalendarView } from './CalendarView';

export const Tasks = () => {
  const { selectedProject } = useSelectedProjectsValue();
  const { projects } = useProjectsValue();
  const { search } = useSearchTasksValue();
  const { filter } = useFilterTasksValue();
  const { view } = useViewTasksValue();

  const { tasks } = useTasks(selectedProject, search, filter);
  const [activeTask, setActiveTask] = useState(0);

  let projectName = '';


  if (projects && selectedProject && !collatedTasksExist(selectedProject)) {
    projectName = getTitle(projects, selectedProject).name;
  }

  if (collatedTasksExist(selectedProject) && selectedProject) {
    projectName = getCollatedTitle(collatedTasks, selectedProject).name;
  }


  useEffect(() => {
    document.title = `${projectName}: Todoist`;
  }, []);

  return (
    <div className="tasks" data-testid="tasks">
      {view === 'calendar' && (
        <CalendarView tasks={tasks} />
      )}
      {view === 'tasks' && (
      <>
        <h2 data-testid="project-name">
          {projectName}
          {' '}
          <SearchTasks />
          <FilterTasks />
        </h2>

        <ul className="tasks__list">
          {tasks.map((task) => (
            <IndiviualTasks
              key={task.id}
              task={task}
              activeTask={activeTask}
              setActiveTask={setActiveTask}
            />
          ))}
        </ul>
        <AddTasks />
      </>
      )}
    </div>
  );
};
