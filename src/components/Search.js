import React from 'react';
import { Input } from 'antd';
import { useSearchTasksValue } from '../context';


export const SearchTasks = () => {
  const { Search } = Input;
  const { setSearch } = useSearchTasksValue();

  return (
    <Search
      placeholder="Search for Task"
      onChange={(e) => setSearch(e.target.value)}
      style={{ width: 200 }}
    />
  );
};
