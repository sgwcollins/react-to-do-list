import React, { useState } from 'react';
import { useSelectedProjectsValue } from '../context/selected-project-context';
import { useProjectsValue } from '../context/project-context';
import { IndiviualProject } from './IndiviualProject';

export const Projects = ({ activeValue = null }) => {
  const [active, setActive] = useState(activeValue);
  const { setSelectedProjects } = useSelectedProjectsValue();
  const { projects } = useProjectsValue();

  return (
    projects
        && projects.map((project) => (
          <li
            key={`${project.id}_${project.docId}`}
            data-doc-id={project.docId}
            data-testid="project-action"
            className={
                    active === project.projectId
                      ? 'active sidebar__project'
                      : 'sidebar__project'
                }
            onClick={() => {
              setActive(project.projectId);
              setSelectedProjects(project.projectId);
            }}
          >
            <IndiviualProject project={project} />
          </li>
        ))
  );
};
