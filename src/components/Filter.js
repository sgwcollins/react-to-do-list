import React from 'react';
import { Select } from 'antd';
import { useFilterTasksValue } from '../context';
import { statusList } from '../constants';
import { StatusBullet } from './StatusBullet';

export const FilterTasks = () => {
  const { Option } = Select;
  const { setFilter } = useFilterTasksValue();
  return (
    <>
      <Select
        onChange={(v) => setFilter(v)}
        style={{ width: 200 }}
        defaultValue=""
      >
        <Option value="">Select Status</Option>
        {statusList.map((status) => (
          <Option key={status.key} value={status.key}>
            {status.name}
            {' '}
            <StatusBullet taskStatus={status.key} />
          </Option>
        ))}
      </Select>
    </>
  );
};
