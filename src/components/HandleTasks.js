import React, { useState } from 'react';
import { DatePicker, Select, Input } from 'antd';
import moment from 'moment';
import { firebase } from '../firebase';
import { useProjectsValue, useSelectedProjectsValue } from '../context';
import { statusList, defaultTask } from '../constants';
import { collatedTasksExist } from '../helpers';

export const HandleTasks = ({
  setShowMain,
  setActiveTask,
  editMode = 'add',
  task = defaultTask,
}) => {

  const [taskContent, setTaskContent] = useState(task.task);
  const [taskTitle, setTaskTitle] = useState(task.taskTitle);
  const [taskDate, setTaskDate] = useState(task.date);
  const [taskStatus, setTaskStatus] = useState(task.status);
  const [project, setProject] = useState(task.projectId);
  const { selectedProject } = useSelectedProjectsValue();
  const { projects } = useProjectsValue();
  const { Option } = Select;

  const addTasks = () => {
    const projectId = project || selectedProject;
    let collatedDate = '';

    if (projectId === 'TODAY') {
      collatedDate = moment().format('DD/MM/YYYY');
    } else if (projectId === 'NEXT_7') {
      collatedDate = moment()
        .add(7, 'days')
        .format('DD/MM/YYYY');
    }

    return (
      taskTitle
            && taskContent
            && projectId
            && firebase
              .firestore()
              .collection('tasks')
              .add({
                archived: false,
                projectId,
                taskTitle,
                task: taskContent,
                status: taskStatus,
                date: collatedDate || taskDate,
                userId: '1',
              })
              .then(() => {
                setTaskContent('');
                setTaskTitle('');
                setTaskDate('');
                setTaskStatus('');
                setProject('');
                setShowMain(false);
              })
    );
  };

  const editTasks = () => {
    const projectId = project || selectedProject;
    const collatedDate = '';

    return (
      taskTitle
            && taskContent
            && projectId
            && firebase
              .firestore()
              .collection('tasks')
              .doc(task.id)
              .update({
                archived: false,
                projectId,
                taskTitle,
                task: taskContent,
                status: taskStatus,
                date: collatedDate || taskDate,
                userId: '1',
              })
              .then(() => {
                setShowMain(false);
                setActiveTask(0);
              })
    );
  };

  return (
    <div className="edit-task__main" data-testid="edit-task-main">
      <label>
        Title:
      </label>
      <br />
      <Input
        className="edit-task__title"
        type="text"
        value={taskTitle}
        onChange={(e) => setTaskTitle(e.target.value)}
      />
      <br />
      <label>
        Content:
      </label>
      <br />
      <Input
        className="edit-task__content"
        type="text"
        value={taskContent}
        onChange={(e) => setTaskContent(e.target.value)}
      />
      <br />
      Date
      <br />
      <DatePicker
        defaultValue={moment(taskDate, 'YYYY-MM-DD')}
        style={{ width: 200 }}
        onChange={(d, dateString) => (setTaskDate(dateString))}
        className="edit-task__date_picker"
      />
      <br />
      Status
      <br />
      <Select
        style={{ width: 120 }}
        placeholder="Select a Status"
        defaultValue={(task.status) ? statusList.find((x) => x.key === task.status).name : ''}
        onChange={(v) => setTaskStatus(v)}
        filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      >
        {statusList.map((status) => <Option value={status.key}>{status.name}</Option>)}
      </Select>
      <br />
      Categories
      <br />
      <Select
        style={{ width: 120 }}
        defaultValue={(!collatedTasksExist(project)) ? project : ''}
        placeholder="Select a Category"
        optionFilterProp="children"
        onChange={(v) => setProject(v)}
        filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      >
        {projects.map((project) => <Option value={project.projectId}>{project.name}</Option>)}
      </Select>
      <br />
      <button
        type="button"
        onClick={() => ((editMode === 'add') ? addTasks() : editTasks())}
        className="edit-task__submit"
        data-testid="edit-task"
      >
        {editMode}
        {' '}
        Tasks
      </button>
      <span
        className="edit-task__cancel"
        data-testid="edit-task-main-cancel"
        onClick={() => {
          setShowMain(false);
        }}
      >
        Cancel
      </span>
    </div>
  );
};
