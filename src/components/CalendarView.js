import React from 'react';
import { Calendar, Badge } from 'antd';
import moment from 'moment';
import { statusList } from '../constants';

export const CalendarView = ({ tasks }) => {
  const getListData = (value) => {
    const listData = [];
    for (const task of tasks) {
      if (value.date() === moment(task.date).date()) {
        const status = statusList.find((status) => status.key === task.status);
        listData.push({ color: status.color, content: task.taskTitle });
      }
    }
    return listData || [];
  };

  const dateCellRender = (value) => {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map((item) => (
          <li key={item.content}>
            <Badge color={item.color} text={item.content} />
          </li>
        ))}
      </ul>
    );
  };


  return (
    <Calendar dateCellRender={dateCellRender} />
  );
};
