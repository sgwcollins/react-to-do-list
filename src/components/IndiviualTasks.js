import React, { useState } from 'react';
import { Tag } from 'antd';
import { CheckboxT } from './Checkbox';
import { StatusBullet } from './StatusBullet';
import { useProjectsValue } from '../context';
import { DaysLeft } from './DaysLeft';
import { getTitle } from '../helpers';
import { HandleTasks } from './HandleTasks';


export const IndiviualTasks = ({
  task,
  activeTask,
  setActiveTask,
}) => {
  const [showMain, setShowMain] = useState(false);
  const { projects } = useProjectsValue();

  let projectName = '';


  if (projects && task.projectId && getTitle(projects, task.projectId)) {
    projectName = getTitle(projects, task.projectId).name;
  }

  if (task.id !== activeTask && showMain) {
    setShowMain(false);
  }


  return (
    <li key={`${task.id}`}>
      {showMain && (
        <>
          <HandleTasks
            setShowMain={setShowMain}
            setActiveTask={setActiveTask}
            editMode="edit"
            task={task}
          />
        </>

      )}

      {!showMain && (
      <>
        <CheckboxT id={`${task.id}`} />
        <span
          onClick={() => {
            setShowMain(true);
            setActiveTask(task.id);
          }}
        >
          <h3 className="task__header">
            {task.taskTitle || 'Title'}
            <StatusBullet taskStatus={task.status} />
          </h3>
          <p>{task.task}</p>
          <Tag color="default">{projectName}</Tag>
          <DaysLeft date={task.date} />
        </span>
      </>
      )}
    </li>
  );
};
