import React, { useState } from 'react';
import { FaTrash,FaCircle } from 'react-icons/fa';
import { firebase } from '../firebase';
import { useProjectsValue, useSelectedProjectsValue } from '../context';

export const IndiviualProject = ({ project }) => {
  const [showConfirm, setShowConfirm] = useState(false);
  const { projects, setProjects } = useProjectsValue();
  const { setSelectedProjects } = useSelectedProjectsValue();

  const deletedProject = (docId) => {
    firebase
      .firestore()
      .collection('projects')
      .docId(docId)
      .delete()
      .then(() => {
        setProjects([...projects]);
        setSelectedProjects('All');
      });
  };

  return (
      <>
          <span className="sidebar__dot"> <FaCircle /> </span>
          <span className="sidebar__project-name">{project.name}</span>
          <span
              className="sidebar__project-delete"
              data-testid="delete-project"
              onClick={()=> setShowConfirm(!showConfirm)}
          >
              <FaTrash/>
              {showConfirm && (
                  <div className="project-delete-modal">
                      <div className="project-delete-modal__inner">
                          <p>Are you sure you want to delete this project</p>
                          <button
                            type="button"
                            onClick={()=>deletedProject(project.docId)}
                          >
                            Delete
                          </button>
                          <span onClick={() => setShowConfirm(!showConfirm)}>Cancel</span>
                      </div>
                  </div>
              )}
          </span>
      </>
  )
};
