import React from 'react';
import { Header } from './components/layout/Header';
import { Content } from './components/layout/Content';
import {
  ProjectProvider,
  SelectedProjectProvider,
  SearchTasksProvider,
  ViewTasksProvider,
  FilterTasksProvider,
} from './context';

import 'antd/dist/antd.css';

export const App = () => (
  <SelectedProjectProvider>
    <ProjectProvider>
      <SearchTasksProvider>
        <FilterTasksProvider>
          <ViewTasksProvider>
            <div className="App" data-testid="application">
              <Header />
              <Content />
            </div>
          </ViewTasksProvider>
        </FilterTasksProvider>
      </SearchTasksProvider>
    </ProjectProvider>
  </SelectedProjectProvider>
);
