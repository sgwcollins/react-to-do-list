
import firebase from 'firebase/app';
import 'firebase/firestore';


const firebaseConfig = firebase.initializeApp({
  apiKey: 'AIzaSyAkpImT6ntFlYF8ztH_-TLuJ35zF1aER08',
  authDomain: 'todolist-3372f.firebaseapp.com',
  databaseURL: 'https://todolist-3372f.firebaseio.com',
  projectId: 'todolist-3372f',
  storageBucket: 'todolist-3372f.appspot.com',
  messagingSenderId: '156929298998',
  appId: '1:156929298998:web:8e72f898ddcd2dc4bb7e97',
  measurementId: 'G-55XQCED5J8',
});

export { firebaseConfig as firebase };
