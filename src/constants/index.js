import moment from 'moment';

export const collatedTasks = [
  { key: 'ALL', name: 'All' },
  { key: 'TODAY', name: 'Today' },
  { key: 'NEXT_7', name: 'Next 7 Days' },
];

export const statusList = [
  { key: 'NOT_STARTED', name: 'Not Started', color: 'blue' },
  { key: 'IN_PROGRESS', name: 'In Progress', color: 'red' },
  { key: 'IN_REVIEW', name: 'In Review', color: 'orange' },
  { key: 'DONE', name: 'Done', color: 'purple' },
];


export const defaultTask = {
  id: 0,
  task: '',
  taskTile: '',
  date: moment().format('YYYY-MM-DD'),
  projectId: '',
  status: '',
};
